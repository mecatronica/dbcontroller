#include <SPI.h>
#include <boards.h>
#include <RBL_nRF8001.h>
#include <services.h>

#define TAM_PACK 20
#define ACK '1'
#define FIN_TRANS '2'
#define OPEN_DOOR '3'
#define NOT_OPEN_DOOR '4'


void setupBLE() { 
 Serial.begin(57600); 
 ble_begin();
}

/* Envia datos por Bluetooth */
void sendToDevice(char data, int lenght) {
  /*int index = 0;
  int i = 0;*/
  
  ble_write(data);
  ble_do_events();
  /*
  while (index < lenght) {
     while (i < TAM_PACK && index < lenght) {    
       ble_write(data[index]);
       index++;
       i++;
     }
     // Enviamos lo anterior, y enviamos el ACK
     ble_do_events();
     ble_write(ACK_SEND);
     ble_do_events();
  }
   */
   
}

/* Recibe datos por Bluetooth.
 * @param data Array en el que guardar los datos recibidos.
 * @param lenght Puntero a un entero en el que guardar el tamaño del array recibido.
 */
int receiveFromDevice(signed char *&data, int *lenght) {
  int indice = 0;
  ble_do_events();
  
  // Si hay datos en el buffer del bluetooth obtenemos el primer mensaje que nos indica la longitud, y el NetworkObject que posterior nos envian.
  if (ble_available()) {
    *lenght = 0;    
    byte hi, lo;
    // Leemos los 2 primeros bytes, que forman un int.
    lo = ble_read();
    ble_do_events();
    hi = ble_read();
    ble_do_events();
    
    // Cogemos 2 bytes, luego tenemos que limpiar los bytes que se añaden (todavia no se el motivo por el cual se añaden cuando enviamos solo 2 bytes).
    int j = 2;
    for(j = 2; j < TAM_PACK; j++)
      ble_read();
      
    // Formamos el int, que en arduino un int son 16 bits, es decir 2 bytes.
    *lenght = hi;
    *lenght = *lenght << 8;
    *lenght = *lenght | lo;
    
    // Reservamos memoria.
    data = (signed char*) malloc(sizeof(signed char)*(*lenght));
    
    // Enviamos el ACK, podemos recibir datos ahora.
    ble_write(ACK);
    Serial.println(*lenght);
    
    // Mientras no hayamos recibido todos los datos no avanzamos.
    while (indice < *lenght) {
      ble_do_events();
      if (ble_available()) {
        int j = 0;
        // Cogemos 20 bytes del buffer y los guardamos en la variable data.
        while(ble_available() && j < TAM_PACK && indice < *lenght) {
          data[indice] = ble_read();       
          Serial.print(indice, DEC);
          Serial.print(" ");
          Serial.println(data[indice]);
          indice++;
          j++;          
        }
        //delay(1000);
        // Cada vez que enviamos 20 bytes, le decimos a la aplicacion de Android que nos envio otros 20.
        if ( j == TAM_PACK)
          ble_write(ACK);
      }
    }  
    ble_write(FIN_TRANS);
    
    /*Serial.print("Indice: ");
    Serial.print(indice);
    Serial.println();
    Serial.println("Adios");*/
    // El ultimo paquete, tiene 0s si es un paquete cuyo contenido es menor de 20 bytes.
    int i = 0;
    while(i < (TAM_PACK - (indice % TAM_PACK))) {
      int basura = ble_read();
      /*Serial.print("BASURA: ");
      Serial.print(basura);
      Serial.print(" ");*/
      i++;   
    }
    
    ble_do_events();
    
    return 0;
  }  
  
  return 1;
  
}

/* Comprueba si el controlador esta recibiendo datos actualmente. */
boolean isConnectedToDevice() { 
 ble_do_events();
 return ble_connected();
}
