#include <SPI.h>
#include <boards.h>
#include <RBL_nRF8001.h>
#include <services.h>


void setup () {
/* Configurar Bluetooth y USB */

  setupBLE();
  setupWifi();

}


void loop () { 
  
/* Esperar conexion Bluetooth */
  if (isConnectedToDevice() ) {
     
      /* Recibir datos Dispositivo */
      int length;
      signed char *data;
      
      // Si hemos recibido bien el pin.
      if (receiveFromDevice(data, &length) == 0) 
      {
        /* Empaquetar datos */
        encapsuleData();
        
        /* Enviar al servidor */
        sendToServerWifi(data, length);
        //sendToServerWifi(data, length);
        Serial.println("ENVIADO");
        /* Recibir respuesta del servidor */
        byte solucion = '3';
        
        Serial.println(length);
        receiveFromServerWifi(&solucion);
        /* Enviar respuesta al dispositivo */
        sendToDevice(solucion, 1);
        /* Abrir o no abrir puerta */
        if (solucion == '3')
          openDoor();
      }
  }
  
}

void encapsuleData() { 

}

void openDoor() {    
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
}


